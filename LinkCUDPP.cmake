###################
# LinkCUDPP.cmake #
###################

# Note: CUDPP_LIBRARIES should point to the *actual* libraries, not the lib directory.
TARGET_LINK_LIBRARIES(${targetname} ${CUDPP_LIBRARIES})
