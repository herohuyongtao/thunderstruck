######################
# SetAppTarget.cmake #
######################

SET(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${thunderstruck_BINARY_DIR}/bin/${targetname})
SET(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG ${thunderstruck_BINARY_DIR}/bin/${targetname})
SET(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE ${thunderstruck_BINARY_DIR}/bin/${targetname})
ADD_EXECUTABLE(${targetname} ${sources} ${headers} ${templates})
INCLUDE(${thunderstruck_SOURCE_DIR}/VCLibraryHack.cmake)

IF(MSVC_IDE)
  SET_TARGET_PROPERTIES(${targetname} PROPERTIES LINK_FLAGS_DEBUG "/DEBUG")
ENDIF()
