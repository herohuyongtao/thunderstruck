/**
 * thunderstruck/tracker: Sequence.cpp
 */

#include "tracker/Sequence.h"

#include <exception>
#include <fstream>
#include <iostream>

#include <boost/algorithm/string/trim.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/regex.hpp>
using boost::format;

namespace thunderstruck {

//#################### CONSTRUCTORS ####################

Sequence::Sequence(const std::string& sequenceDir)
: m_sequencePath(sequenceDir)
{
  std::cout << "Reading sequence data from " << canonical(m_sequencePath) << '\n';
  std::cout << "Sequence name: " << name() << '\n';

  read_frames_file();
  read_ground_truth_file();
}

//#################### PUBLIC MEMBER FUNCTIONS ####################

const cv::Rect_<float>& Sequence::bounding_box(size_t i) const
{
  return m_boundingBoxes[i];
}

cv::Mat Sequence::frame(size_t i) const
{
  std::string filename = str(format("img%05d.png") % (m_firstFrameNumber + i));
  return cv::imread((m_sequencePath / "imgs" / filename).string().c_str());
}

size_t Sequence::frame_count() const
{
  return m_frameCount;
}

std::string Sequence::name() const
{
  return m_sequencePath.parent_path().leaf().generic_string();
}

//#################### PRIVATE MEMBER FUNCTIONS ####################

void Sequence::read_frames_file()
{
  boost::filesystem::path framesPath = m_sequencePath / (name() + "_frames.txt");
  std::ifstream fs(framesPath.c_str());
  if(fs.fail())
  {
    throw std::runtime_error("Could not read sequence frames file");
  }

  std::string line;
  std::getline(fs, line);
  boost::trim(line);
  boost::regex expr("(\\d+),(\\d+)");
  boost::smatch what;
  if(regex_match(line, what, expr))
  {
    int startFrame = boost::lexical_cast<int>(what[1]);
    int endFrame = boost::lexical_cast<int>(what[2]);
    m_firstFrameNumber = startFrame;
    m_frameCount = endFrame + 1 - startFrame;
  }
  else
  {
    throw std::runtime_error("Could not parse sequence frame range");
  }
}

void Sequence::read_ground_truth_file()
{
  boost::filesystem::path gtPath = m_sequencePath / (name() + "_gt.txt");
  std::ifstream fs(gtPath.c_str());
  if(fs.fail())
  {
    throw std::runtime_error("Could not read sequence ground truth file");
  }

  std::string line;
  while(std::getline(fs, line))
  {
    boost::trim(line);
    if(line == "") continue;

    std::string real = "(-?\\d+(?:\\.\\d+)?)";
    boost::regex expr(real + "," + real + "," + real + "," + real);
    boost::smatch what;
    if(regex_match(line, what, expr))
    {
      // The captured values are xMin, yMin, width and height, in that order.
      float values[4];
      for(size_t i = 0; i < 4; ++i)
      {
        values[i] = boost::lexical_cast<float>(what[i+1]);
      }
      m_boundingBoxes.push_back(cv::Rect_<float>(values[0], values[1], values[2], values[3]));
    }
    else
    {
      throw std::runtime_error("Could not parse one of the ground truth bounding boxes");
    }
  }
}

}
