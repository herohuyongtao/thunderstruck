//###
#if 1

#include <algorithm>
#include <cmath>
#include <iostream>

#include <cudpp.h>

#include <thunderstruck/util/GPUVector.h>
using namespace thunderstruck;

int main()
{
  // Initialise the CUDPP library.
  CUDPPHandle cudpp;
  cudppCreate(&cudpp);

  // Set up the input array.
  const int N = 100;
  std::vector<double> cpuInput(N);
  for(int i = 0; i < N; ++i)
  {
    double dx = fabs(i - 50);
    cpuInput[i] = dx * dx;
  }

  std::copy(cpuInput.begin(), cpuInput.end(), std::ostream_iterator<double>(std::cout, " "));
  std::cout << "\n\n";

  GPUVector<double> gpuInput(cpuInput);

  // Set up the output array to which to write the results.
  GPUVector<double> gpuOutput(1);

  // Run the reduction.
  CUDPPConfiguration config;
  config.op = CUDPP_MIN;
  config.datatype = CUDPP_DOUBLE;
  config.algorithm = CUDPP_REDUCE;
  config.options = CUDPP_OPTION_FORWARD | CUDPP_OPTION_INCLUSIVE;

  CUDPPHandle plan = 0;
  CUDPPResult result = cudppPlan(cudpp, &plan, config, N, 1, 0);
  if(result != CUDPP_SUCCESS)
  {
    std::cout << "Failed to create CUDPP plan!\n";
    return EXIT_FAILURE;
  }

  result = cudppReduce(plan, gpuOutput.get(), gpuInput.get(), N);
  if(result != CUDPP_SUCCESS)
  {
    std::cout << "Failed to run CUDPP reduction!\n";
    return EXIT_FAILURE;
  }

  // Output the results.
  gpuOutput.output(std::cout);

  // Destroy the created plan.
  cudppDestroyPlan(plan);

  // Shut down the CUDPP library.
  cudppDestroy(cudpp);

  return 0;
}

#endif

//###
#if 0
#include <algorithm>
#include <iostream>

#include <cudpp.h>

#include <thunderstruck/util/GPUVector.h>
using namespace thunderstruck;

int main()
{
  // Initialise the CUDPP library.
  CUDPPHandle cudpp;
  cudppCreate(&cudpp);

  // Set up the input array on which to run the segmented scan.
  const int SEGMENT_COUNT = 4;
  const int SEGMENT_SIZE = 8;
  const int TOTAL_SIZE = SEGMENT_COUNT * SEGMENT_SIZE;
  std::vector<float> cpuInput(TOTAL_SIZE);
  for(size_t i = 0; i < SEGMENT_COUNT; ++i)
  {
    for(size_t j = 0; j < SEGMENT_SIZE; ++j)
    {
      cpuInput[i * SEGMENT_SIZE + j] = i + 1;
    }
  }

  std::copy(cpuInput.begin(), cpuInput.end(), std::ostream_iterator<float>(std::cout, " "));
  std::cout << "\n\n";

  GPUVector<float> gpuInput(cpuInput);

  // Set up the output array to which to write the results.
  GPUVector<float> gpuOutput(TOTAL_SIZE);

  // Run the segmented scan.
  CUDPPConfiguration config;
  config.op = CUDPP_ADD;
  config.datatype = CUDPP_FLOAT;
  config.algorithm = CUDPP_SCAN;
  config.options = CUDPP_OPTION_FORWARD | CUDPP_OPTION_INCLUSIVE;

  CUDPPHandle plan = 0;
  CUDPPResult result = cudppPlan(cudpp, &plan, config, TOTAL_SIZE, SEGMENT_COUNT, SEGMENT_SIZE);
  if(result != CUDPP_SUCCESS)
  {
    std::cout << "Failed to create CUDPP plan!\n";
    return EXIT_FAILURE;
  }

  result = cudppMultiScan(plan, gpuOutput.get(), gpuInput.get(), SEGMENT_SIZE, SEGMENT_COUNT);
  if(result != CUDPP_SUCCESS)
  {
    std::cout << "Failed to run CUDPP multi-scan!\n";
    return EXIT_FAILURE;
  }

  // Output the results.
  std::vector<float> cpuOutput = gpuOutput.to_cpu();
  std::copy(cpuOutput.begin(), cpuOutput.end(), std::ostream_iterator<float>(std::cout, " "));
  std::cout << '\n';

  // Destroy the created plan.
  cudppDestroyPlan(plan);

  // Shut down the CUDPP library.
  cudppDestroy(cudpp);

  return 0;
}
#endif
